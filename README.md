<h2>This program was build using :
    Frontend : HTML + CSS
    Backend : Go Language</h2>
    
<b>This program was simple bank scenario who can deposit, look for transaction history through the database</b>

For testing this code you can download all of the file in this project and run it
To run this program, at first you have to install go language on your PC

<b>After Go language installed on your PC run this command :</b>
<div class="row">
go get golang.org/x/crypto/bcrypt

go get github.com/go-sql-driver/mysql
</div>
both command required for running this program

There's a bank_db for testing this program, just change the database direction in main.go inside main function
<div class="row">
db, err = sql.Open("mysql", "root:@/bank_db")

change it to


db, err = sql.Open("mysql", "yourDbUsername:yourDbPassword"@/bank_db")
</div>

<b>To run this program, download all of the resource file, run command above in terminal, after that run this command in terminal :</b>
<div class="row">
go run main.go
</div>





<b>To look the scenario of this program you can open the ER Diagram file</b>

Thank you :)