package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"

import "golang.org/x/crypto/bcrypt"

import "net/http"
import "strconv"

var db *sql.DB
var err error

func signupPage(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.ServeFile(res, req, "signup.html")
		return
	}
	// take the value from the form
	username := req.FormValue("username")
	password := req.FormValue("password")
	first_name := req.FormValue("first_name")
	last_name := req.FormValue("last_name")

	// create variable as a vessel from database
	var user string
	// scan the username from table users
	err := db.QueryRow("SELECT username FROM users WHERE username=?", username).Scan(&user)

	switch {
	case err == sql.ErrNoRows:
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			http.Error(res, "Server error, unable to create your account.", 500)
			return
		}
		//if there's no error new member would be sign up into database
		_, err = db.Exec("INSERT INTO users(username, password, first_name, last_name) VALUES(?, ?, ?, ?)", 
			username, hashedPassword, first_name, last_name)
		if err != nil {
			http.Error(res, "Server error, unable to create your account.", 500)
			return
		}

		// if all succeded it will redirect to home page
		http.Redirect(res, req, "/", 301)
		return
	case err != nil:
		http.Error(res, "Server error, unable to create your account.", 500)
		return
	default:
		http.Redirect(res, req, "/", 301)
	}
}

func depositNonPage(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.ServeFile(res, req, "deposit_non_member.html")
		return
	}
	// take the value from the form
	username := req.FormValue("username")
	first_name := req.FormValue("first_name")
	last_name := req.FormValue("last_name")
	deposit := req.FormValue("deposit")

	var user string

	err := db.QueryRow("SELECT username FROM users WHERE username=?", username).Scan(&user)

	switch {
	case err == sql.ErrNoRows:

		_, err = db.Exec("INSERT INTO users(username, first_name, last_name, deposit, balance) VALUES(?, ?, ?, ?, ?)", 
			username, first_name, last_name, deposit, deposit)
		if err != nil {
			http.Error(res, "Server error, unable to deposit your money.", 500)
			return
		}
		// throw it to transaction history, so superuser can check it
		_, err = db.Exec("INSERT INTO history(username, deposit, balance) VALUES(?, ?, ?)", 
			username, deposit, deposit)
		if err != nil {
			http.Error(res, "Server error, unable to deposit your money.", 500)
			return
		}

		http.Redirect(res, req, "/", 301)
		return
	case err != nil:
		http.Error(res, "Server error, unable to deposit your money", 500)
		return
	default:
		http.Redirect(res, req, "/", 301)
	}
}

func loginPage(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.ServeFile(res, req, "login.html")
		return
	}

	username := req.FormValue("username")
	password := req.FormValue("password")

	var databaseUsername string
	var databasePassword string

	err := db.QueryRow("SELECT username, password FROM users WHERE username=?", username).Scan(&databaseUsername, &databasePassword)

	if err != nil {
		http.Redirect(res, req, "/login", 301)
		return
	}
	// compare the password, if it success it will redirect to member deposit page
	err = bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(password))
	if err != nil {
		http.Redirect(res, req, "/login", 301)
		return
	}

	http.Redirect(res, req, "/deposit_member", 301)
	return

}

func memberDepositPage(res http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.ServeFile(res, req, "deposit_member.html")
		return
	}

	username := req.FormValue("username")
	password := req.FormValue("password")
	deposit := req.FormValue("deposit")

	var databaseUsername string
	var databasePassword string
	var databaseBalance int
	var sum int

	err := db.QueryRow("SELECT username, password, balance FROM users WHERE username=?", username).Scan(&databaseUsername, &databasePassword, &databaseBalance)

	if err != nil {
		http.Redirect(res, req, "/login", 301)
		return
	}
	// log in again to make sure the right user
	err = bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(password))
	if err != nil {
		http.Redirect(res, req, "/login", 301)
		return
	}
	// to sum the balance of member
	fix, err := strconv.Atoi(deposit)
	sum = databaseBalance + fix
	_, err = db.Exec("UPDATE users set balance=?, deposit=? WHERE username=?", 
			sum, deposit, username)
	if err != nil {
			http.Error(res, "Server error, unable to deposit your money.", 500)
			return
	}
	// throw it to transaction history so superuser can check the history
	_, err = db.Exec("INSERT INTO history(username, deposit, balance) VALUES(?, ?, ?)", 
			username, deposit, sum)
	if err != nil {
			http.Error(res, "Server error, unable to deposit your money.", 500)
			return
	}

	http.Redirect(res, req, "/", 301)
	return

}

func homePage(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "index.html")
}

func main() {
	db, err = sql.Open("mysql", "root:@/bank_db")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	http.HandleFunc("/signup", signupPage)
	http.HandleFunc("/login", loginPage)
	http.HandleFunc("/deposit_non", depositNonPage)
	http.HandleFunc("/deposit_member", memberDepositPage)
	http.HandleFunc("/", homePage)
	http.ListenAndServe(":8080", nil)
}