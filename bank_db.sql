-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2019 at 02:21 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `username` varchar(50) NOT NULL,
  `balance` int(50) NOT NULL,
  `deposit` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`username`, `balance`, `deposit`) VALUES
('farhan', 1065000, 90000),
('farhansmg@gmail.com', 100000, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `password` varchar(120) NOT NULL,
  `balance` int(255) NOT NULL,
  `deposit` int(255) NOT NULL,
  `account_number` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `first_name`, `last_name`, `password`, `balance`, `deposit`, `account_number`) VALUES
('rayhan', 'rayhan', 'babl', '$2a$10$413jiXsM7t/4McRN/PRjKOFrCefCXudAf4rRfET4qRWXYY0itjIYy', 0, 0, 0),
('jkds', 'kjdlsajkl', 'jdsklajk', '$2a$10$RT/kntSrD2eYmexTmPw8ye5TMwyiDcBTsZVxuXtMFGjKT73VY79.e', 0, 0, 0),
('dsadsa', 'dsadsa', 'sadsa', '$2a$10$n4hR1X2Iv54m0BbdyYVsdeaWc8S.8l9gYk7V/Uqq/aS2HZeyBa7ma', 0, 0, 0),
(',mn,mn', 'kljlk', 'jkllkkjld', '$2a$10$9mJk1H/nD05oGCOIiNI5pOIxiyEjH8jyrT9XCz5ks5uLINTgoSeZ6', 0, 0, 0),
('bbjhbj', 'dsads', 'hhuiiuhiu', '$2a$10$sgmdYT6d71B8ivneubryCugGe1qT4ywcmvLmwGWZUaBmtYtqkpxc6', 0, 0, 0),
('djskadbs', 'nfsdoonsoda', 'cdskjisjad', '$2a$10$43dROJ1cW/0j944HCG2MkOmobPoCMlnFl4NvNUDRBby89fNJHGFx6', 0, 0, 0),
('jkdsabkj', 'dsadsa', 'jknkjkj', '$2a$10$FQ/wCGDniiwemR.X78wG5eyX/xPJFR0eK80z7W1kHxSJcPA.iM.oC', 0, 0, 0),
('farhan', 'farhan', 'pratam', '$2a$10$m7mWlAZYT469NdVUXb3AJeNxwcEfsNAfvpZ.eEHGWj3GV6wesffhC', 1065000, 90000, 0),
('', 'test1', 'sds', '', 100000, 100000, 0),
('farhansmg@gmail.com', 'Farhan', 'Pratama', '', 100000, 100000, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
